import time
from base64 import b64encode
import requests
import pytest
import lorem

# GLOBAL VARIABLES
blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
comments_endpoint_url = blog_url + "/wp-json/wp/v2/comments"
user_endpoint_url = blog_url + "/wp-json/wp/v2/users"
# editor
username_e = 'editor'
password_e = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
token_e = b64encode(f"{username_e}:{password_e}".encode('utf-8')).decode("ascii")
editor_name = "John Editor"
# commenter
username_c = 'commenter'
password_c = 'SXlx hpon SR7k issV W2in zdTb'
token_c = b64encode(f"{username_c}:{password_c}".encode('utf-8')).decode("ascii")
commenter_name = "Albert Commenter"


@pytest.fixture(scope='module')
def article_e():
    timestamp = int(time.time())
    article = {
        "article_creation_date": timestamp,
        "article_title": "This is new post " + str(timestamp),
        "article_subtitle": lorem.sentence(),
        "article_text": lorem.paragraph()
    }
    return article


@pytest.fixture(scope='module')
def headers_e():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_e
    }
    return headers


@pytest.fixture(scope='module')
def posted_article_e(article_e, headers_e):
    payload = {
        "title": article_e["article_title"],
        "excerpt": article_e["article_subtitle"],
        "content": article_e["article_text"],
        "status": "publish"
    }
    response = requests.post(url=posts_endpoint_url, headers=headers_e, json=payload)
    return response


@pytest.fixture(scope='module')
def comment_c_to_article_e(posted_article_e):
    timestamp = int(time.time())
    comment = {
        "comment_creation_date": timestamp,
        "comment_post_rel": posted_article_e.json()["id"],
        "comment_text": lorem.paragraph()
    }
    return comment


@pytest.fixture(scope='module')
def headers_c():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_c
    }
    return headers


@pytest.fixture(scope='module')
def posted_comment_c(comment_c_to_article_e, headers_c):
    payload = {
        "content": comment_c_to_article_e["comment_text"],
        "post": comment_c_to_article_e["comment_post_rel"],
        "status": "publish"
    }
    response = requests.post(url=comments_endpoint_url, headers=headers_c, json=payload)
    return response


@pytest.fixture(scope='module')
def comment_e_to_comment_c(posted_comment_c):
    timestamp = int(time.time())
    comment = {
        "comment_creation_date": timestamp,
        "comment_post_rel": posted_comment_c.json()["post"],
        "comment_parent_rel": posted_comment_c.json()["id"],
        "comment_text": lorem.paragraph()
    }
    return comment


@pytest.fixture(scope='module')
def posted_comment_e(comment_e_to_comment_c, headers_e):
    payload = {
        "content": comment_e_to_comment_c["comment_text"],
        "post": comment_e_to_comment_c["comment_post_rel"],
        "parent": comment_e_to_comment_c["comment_parent_rel"],
        "status": "publish"
    }
    response = requests.post(url=comments_endpoint_url, headers=headers_e, json=payload)
    return response


# CREATE POST - first step
def test_new_post_is_successfully_created(posted_article_e):
    # Status check
    assert posted_article_e.status_code == 201
    assert posted_article_e.reason == "Created"
    # User check (must be Editor)
    user_id = posted_article_e.json()["author"]
    wordpress_user_url = f'{user_endpoint_url}/{user_id}'
    published_article = requests.get(url=wordpress_user_url)
    assert published_article.json()["name"] == editor_name


# CREATE COMMENT for post - second step
def test_comment_for_post(posted_comment_c, posted_article_e):
    # Status check
    assert posted_comment_c.status_code == 201
    assert posted_comment_c.reason == "Created"
    # User check (must be Commenter)
    user_id = posted_comment_c.json()["author"]
    wordpress_user_url = f'{user_endpoint_url}/{user_id}'
    published_comment = requests.get(url=wordpress_user_url)
    assert published_comment.json()["name"] == commenter_name
    # Relation to post check
    post_id = posted_article_e.json()["id"]
    assert posted_comment_c.json()["post"] == post_id


# CREATE COMMENT for parent comment and post - third step
def test_comments_post_final_relations(posted_comment_c, posted_article_e, posted_comment_e):
    # Status check
    assert posted_comment_e.status_code == 201
    assert posted_comment_e.reason == "Created"
    # User check (must be Editor)
    user_id = posted_comment_e.json()["author"]
    wordpress_user_url = f'{user_endpoint_url}/{user_id}'
    published_comment = requests.get(url=wordpress_user_url)
    assert published_comment.json()["name"] == editor_name
    # Relations final check
    post_id = posted_article_e.json()["id"]
    parent_comment_id = posted_comment_c.json()["id"]
    assert posted_comment_e.json()["post"] == post_id
    assert posted_comment_e.json()["parent"] == parent_comment_id

